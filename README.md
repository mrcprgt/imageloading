# Image Loading App for Finastra Technical Exam

# About the App
It is an app that has a Bottom Navigation View with 2 screens. First screen is a screen that has an endless scrolling recycler view with Curated Photos from the Pexels API. It also has a swipe to refresh functionality that will clear the list and pull new random photos each time.
The second screen is a sample trial of a viewpager that will load videos from the Pexels API. Endless scrolling is not supported in this one.

# Tools Used:

- RecyclerView
- Android Core Lifecycle
- Retrofit2
- Mockito
- Material UI
- Constraint Layout
- Coroutines
- GSON
- Pexels API - (https://www.pexels.com/api/documentation)

