package com.mrcprgt.imageloading

import com.google.gson.Gson
import com.mrcprgt.imageloading.data.network.PexelsApi
import com.mrcprgt.imageloading.data.network.PexelsRepository
import com.mrcprgt.imageloading.data.network.RemotePexelCuratedPhotos
import com.mrcprgt.imageloading.data.network.RemotePopularPexelVideos
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

class PexelRepositoryTest {
    private lateinit var repo: PexelsRepository

    @Mock
    lateinit var api: PexelsApi

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        repo = PexelsRepository(api)

    }

    @Test
    fun test() = runBlocking {
        val asset = Utility.readFromOwnAssets(
            module = "app",
            filenameWithExtension = "remote_photos.json"
        )


        Mockito.`when`(
            api.getCuratedImages(1)
        ).thenReturn(
            Response.success(
                Gson().fromJson(asset, RemotePexelCuratedPhotos::class.java)
            )
        )

        val response = repo.getCuratedPexelPhotos(1)

        Assert.assertEquals("Simge Tek", response.body()?.photos?.first()?.photographer)
    }

    @Test
    fun testGetRandomPhoto() = runBlocking {
        val asset = Utility.readFromOwnAssets(
            module = "app",
            filenameWithExtension = "remote_videos.json"
        )

        Mockito.`when`(
            api.getPopularVideos(1)
        ).thenReturn(
            Response.success(
                Gson().fromJson(asset, RemotePopularPexelVideos::class.java)
            )
        )

        repo.getRandomPexelPhotos(1)

        val response = repo.getPopularVideos(1)

        Assert.assertEquals("Ruvim Miksanskiy", response.body()?.videos?.first()?.user?.name)
    }
}