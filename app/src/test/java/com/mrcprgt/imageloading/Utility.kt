package com.mrcprgt.imageloading

import android.content.Context
import com.google.gson.Gson
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader

object Utility {
    fun <T> fromJsonFile(context: Context, filePathInAssets: String, classType: Class<T>?) : T {
        val file = context.assets.open(filePathInAssets).bufferedReader().use {
            it.readText()
        }
        val convertedClass = Gson().fromJson(file, classType)
        return convertedClass as T
    }

    fun readFromOwnAssets(
        module : String,
        filenameWithExtension: String
    ): String {
        try {
            val br = BufferedReader(InputStreamReader(FileInputStream("../${module}/src/main/assets/$filenameWithExtension")))
            val sb = StringBuilder()

            var line: String? = br.readLine()
            while (line != null) {
                sb.append(line)
                line = br.readLine()
            }
            return sb.toString()
        } catch (e: IOException) {
            e.printStackTrace()
            throw IllegalStateException(e.message)
        }
    }

}