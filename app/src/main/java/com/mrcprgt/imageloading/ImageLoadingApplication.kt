package com.mrcprgt.imageloading

import android.app.Application
import com.mrcprgt.imageloading.di.DependencyInjector

class ImageLoadingApplication: Application() {
    val dependencyInjector = DependencyInjector()
}