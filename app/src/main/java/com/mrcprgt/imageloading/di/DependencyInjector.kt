package com.mrcprgt.imageloading.di

import com.mrcprgt.imageloading.data.network.PexelsApi
import com.mrcprgt.imageloading.data.network.PexelsRepository
import com.mrcprgt.imageloading.presentation.MainViewModel
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DependencyInjector {
    private val pexelsApi = Retrofit.Builder()
        .baseUrl(PexelsApi.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(OkHttpClient().newBuilder().addInterceptor(HeaderInterceptor()).build())
        .build()
        .create(PexelsApi::class.java)

    private val photoRepository = PexelsRepository(pexelsApi)

    val mainViewModelFactory = MainViewModelFactory(photoRepository)
}


class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        proceed(
            request()
                .newBuilder()
                .addHeader("Authorization", "8aWMIeSvUtSacjXAKvflxuQpfoIOMcG9XZryafZDcwEjJXR5gsuK9jAg")
                .build()
        )
    }
}

// Definition of a Factory interface with a function to create objects of a type
interface Factory<T> {
    fun create(): T
    fun provide() : T
}

class MainViewModelFactory(private val pexelsRepository: PexelsRepository) : Factory<MainViewModel> {
    private var viewModel : MainViewModel? = null
    override fun create(): MainViewModel {
        viewModel = MainViewModel(pexelsRepository)
        return viewModel!!
    }

    override fun provide(): MainViewModel {
        return viewModel ?: throw Exception("Viewmodel not initialized")
    }
}