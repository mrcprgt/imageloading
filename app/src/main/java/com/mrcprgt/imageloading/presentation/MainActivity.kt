package com.mrcprgt.imageloading.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.mrcprgt.imageloading.ImageLoadingApplication
import com.mrcprgt.imageloading.R
import com.mrcprgt.imageloading.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private lateinit var viewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val dependencyInjector = (application as ImageLoadingApplication).dependencyInjector
        viewModel = dependencyInjector.mainViewModelFactory.create()

        setContentView(binding.root)
        setCurrentFragment(ListFragment())
        binding.bottomNavigationView.selectedItemId = R.id.list
        binding.bottomNavigationView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.list -> setCurrentFragment(ListFragment())
                R.id.carousel -> setCurrentFragment(VideoFragment())
            }
            true
        }
    }

    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frameLayout, fragment)
            commit()
        }
}