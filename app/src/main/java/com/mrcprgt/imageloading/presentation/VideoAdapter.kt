package com.mrcprgt.imageloading.presentation

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mrcprgt.imageloading.databinding.ItemVideoListingBinding
import com.mrcprgt.imageloading.domain.model.PexelVideo

class VideoAdapter() :
    RecyclerView.Adapter<VideoAdapter.VideoViewHolder>() {

    var data: MutableList<PexelVideo> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        val binding = ItemVideoListingBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return VideoViewHolder(binding)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        val photo = data[position]
        holder.bind(photo)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun add(newData: MutableList<PexelVideo>) {
        data.addAll(newData)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clear() {
        data.clear()
        notifyDataSetChanged()
    }

    inner class VideoViewHolder(private val view: ItemVideoListingBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(video: PexelVideo) {
            view.videoView.setVideoPath(video.link)
            view.videoView.setOnPreparedListener {
                view.videoProgressBar.visibility = View.VISIBLE
                it.start()

//                val videoRate: Float = (it.videoWidth / it.videoHeight).toFloat()
//                val screenRatio: Float = (view.videoView.width / view.videoView.height).toFloat()
//                val scale: Float = videoRate / screenRatio
//
//                if (scale >= 1f) {
//                    view.videoView.scaleX = scale
//                } else {
//                    view.videoView.scaleY = (1f / scale)
//                }
            }

            view.videoView.setOnCompletionListener {
                it.start()
            }
            view.textVideoTitle.text = video.author
            view.videoView
        }
    }

}