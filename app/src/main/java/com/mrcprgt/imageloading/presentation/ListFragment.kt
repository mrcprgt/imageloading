package com.mrcprgt.imageloading.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.mrcprgt.imageloading.ImageLoadingApplication
import com.mrcprgt.imageloading.databinding.FragmentListBinding
import com.mrcprgt.imageloading.util.Resource
import kotlinx.coroutines.launch

class ListFragment: Fragment() {

    private val binding: FragmentListBinding by lazy {
        FragmentListBinding.inflate(layoutInflater)
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: PhotoAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dependencyInjector = (requireActivity().application as ImageLoadingApplication).dependencyInjector
        viewModel = dependencyInjector.mainViewModelFactory.provide()

        adapter = PhotoAdapter()
        viewModel.fetchCuratedPhotos(1)
        binding.photosRecyclerView.layoutManager =
            GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)
        binding.photosRecyclerView.adapter = adapter
        binding.photosRecyclerView.addOnScrollListener(object : PhotoRecyclerViewEndlessListener() {
            override fun onLoadMore(page: Int) {
                viewModel.fetchCuratedPhotos(page)
            }

        })

        binding.swipeRefreshLayout.setOnRefreshListener {
            lifecycleScope.launch {
                adapter.clear()
                viewModel.clearPhotos()
                viewModel.handleRefresh()
                binding.swipeRefreshLayout.isRefreshing = false
                binding.photosRecyclerView.clearOnScrollListeners()
                binding.photosRecyclerView.addOnScrollListener(object : PhotoRecyclerViewEndlessListener() {
                    override fun onLoadMore(page: Int) {
                        viewModel.handleRefresh()
                    }

                })
            }
        }

        observeUI()

    }

    private fun observeUI() {
        viewModel.photos.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Error -> TODO()
                is Resource.Loading -> {
                    binding.swipeRefreshLayout.isRefreshing = true
                }

                is Resource.Success -> {
                    binding.swipeRefreshLayout.isRefreshing = false
                    val data = it.data
                    adapter.add(data ?: mutableListOf())
                }
            }
        }
    }
}