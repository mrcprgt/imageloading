package com.mrcprgt.imageloading.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mrcprgt.imageloading.data.mapper.toDomain
import com.mrcprgt.imageloading.data.network.PexelsRepository
import com.mrcprgt.imageloading.domain.model.PexelPhoto
import com.mrcprgt.imageloading.domain.model.PexelVideo
import com.mrcprgt.imageloading.util.Resource
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainViewModel(
    private val repository: PexelsRepository
) : ViewModel() {

    private var _photos: MutableLiveData<Resource<MutableList<PexelPhoto>>> = MutableLiveData()
    private var _videos: MutableLiveData<Resource<MutableList<PexelVideo>>> = MutableLiveData()
    val photos get() = _photos
    val videos get() = _videos

    var cachedPhotoList: MutableList<PexelPhoto>? = null
    var cachedVideoList: MutableList<PexelVideo>? = null

    var viewModelPage = 1

    init {
        fetchCuratedPhotos(viewModelPage)
    }
    fun fetchCuratedPhotos(page: Int) {
        viewModelScope.launch {
            _photos.postValue(Resource.Loading())
             delay(2000L)
            try {
                val response = repository.getCuratedPexelPhotos(page)

                if (response.isSuccessful) {
                    response.body()?.let { resultResponse ->
                        viewModelPage++
                        if (cachedPhotoList == null) {
                            cachedPhotoList = resultResponse.toDomain().toMutableList()
                        } else {
                            val toCachePhotoList = resultResponse.toDomain().toMutableList()

                            cachedPhotoList?.addAll(toCachePhotoList)
                        }
                        _photos.postValue(Resource.Success(resultResponse.toDomain().toMutableList()))
                    }
                }
            } catch (e: Exception) {
                _photos.postValue(Resource.Error(e.localizedMessage))
            }
        }
    }

    fun handleRefresh() {
        viewModelScope.launch {
            _photos.postValue(Resource.Loading())
            try {
                val response = repository.getRandomPexelPhotos(viewModelPage)

                if (response.isSuccessful) {
                    response.body()?.let { resultResponse ->
                        viewModelPage++
                        if (cachedPhotoList == null) {
                            cachedPhotoList = resultResponse.toDomain().toMutableList()
                        } else {
                            val toCachePhotoList = resultResponse.toDomain().toMutableList()

                            cachedPhotoList?.addAll(toCachePhotoList)
                        }
                        _photos.postValue(Resource.Success(cachedPhotoList))
                    }
                }
            } catch (e: Exception) {
                _photos.postValue(Resource.Error(e.localizedMessage))
            }
        }
    }

    fun clearPhotos() {
        cachedPhotoList?.clear()
    }

    fun getVideos() {
        viewModelScope.launch {
            _photos.postValue(Resource.Loading())
            delay(2000L)
            try {
                val response = repository.getPopularVideos(1)

                if (response.isSuccessful) {
                    response.body()?.let { resultResponse ->
                        if (cachedVideoList == null) {
                            cachedVideoList = resultResponse.toDomain().toMutableList()
                        } else {
                            val toCache = resultResponse.toDomain().toMutableList()

                            cachedVideoList?.addAll(toCache)
                        }
                        _videos.postValue(Resource.Success(cachedVideoList))
                    }
                }
            } catch (e: Exception) {
                _photos.postValue(Resource.Error(e.localizedMessage))
            }
        }
    }
}