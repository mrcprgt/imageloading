package com.mrcprgt.imageloading.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mrcprgt.imageloading.ImageLoadingApplication
import com.mrcprgt.imageloading.databinding.FragmentCarouselBinding
import com.mrcprgt.imageloading.util.Resource

class VideoFragment: Fragment() {

    private val binding: FragmentCarouselBinding by lazy {
        FragmentCarouselBinding.inflate(layoutInflater)
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: VideoAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dependencyInjector = (requireActivity().application as ImageLoadingApplication).dependencyInjector
        viewModel = dependencyInjector.mainViewModelFactory.provide()

        adapter = VideoAdapter()
        viewModel.getVideos()

        binding.videoViewPager.adapter = adapter


        observeUI()

    }

    private fun observeUI() {
        viewModel.videos.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Error -> TODO()
                is Resource.Loading -> {

                }

                is Resource.Success -> {
                    val data = it.data
                    adapter.add(data ?: mutableListOf())
                }
            }
        }
    }
}