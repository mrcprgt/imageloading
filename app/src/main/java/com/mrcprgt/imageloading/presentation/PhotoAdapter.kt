package com.mrcprgt.imageloading.presentation

import android.annotation.SuppressLint
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mrcprgt.imageloading.databinding.ItemImageBinding
import com.mrcprgt.imageloading.domain.model.PexelPhoto

class PhotoAdapter() :
    RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder>() {

    var data: MutableList<PexelPhoto> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val binding = ItemImageBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return PhotoViewHolder(binding)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val photo = data[position]
        holder.bind(photo)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun add(newData: MutableList<PexelPhoto>) {
        data.addAll(newData)
        Log.e("LOG", data.size.toString())
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clear() {
        data.clear()
        notifyDataSetChanged()
    }

    inner class PhotoViewHolder(private val view: ItemImageBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(photo: PexelPhoto) {
            Glide
                .with(view.root)
                .load(photo.src)
                .into(view.itemImageView)

            view.cardView.setCardBackgroundColor(Color.parseColor(photo.avgColor))
            view.titleTextView.text = photo.altText.takeIf {
                it.isNotEmpty()
            } ?: "No title given"
            view.artistTextView.text = photo.photographer
        }
    }

}