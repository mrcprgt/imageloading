package com.mrcprgt.imageloading.data.network

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import kotlin.random.Random

class PexelsRepository(
    private val api: PexelsApi
) {
    suspend fun getCuratedPexelPhotos(page: Int): Response<RemotePexelCuratedPhotos> = withContext(
        Dispatchers.IO
    ) {
        api.getCuratedImages(page)
    }

    suspend fun getRandomPexelPhotos(page: Int): Response<RemotePexelCuratedPhotos> = withContext(
        Dispatchers.IO
    ) {
        val choices = listOf("cat", "dog", "bird", "people", "games", "ocean", "mountains")

        api.getRandomImages(page, choices[Random.nextInt(choices.size)])
    }

    suspend fun getPopularVideos(page: Int): Response<RemotePopularPexelVideos> = withContext(
        Dispatchers.IO
    ) {
        api.getPopularVideos(page)
    }
}