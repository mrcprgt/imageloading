package com.mrcprgt.imageloading.data.mapper

import com.mrcprgt.imageloading.data.network.RemotePexelCuratedPhotos
import com.mrcprgt.imageloading.data.network.RemotePopularPexelVideos
import com.mrcprgt.imageloading.domain.model.PexelPhoto
import com.mrcprgt.imageloading.domain.model.PexelVideo

fun RemotePexelCuratedPhotos.toDomain() : List<PexelPhoto> {
    return this.photos.map {
        PexelPhoto(
            id = it.id,
            photographer = it.photographer,
            src = it.src.medium,
            avgColor = it.avgColor,
            altText = it.alt

        )
    }
}

fun RemotePopularPexelVideos.toDomain() : List<PexelVideo> {
    return this.videos.map {
        PexelVideo(
            id = it.id,
            author = it.user.name,
            link = it.videoFiles.first().link
        )
    }
}
