package com.mrcprgt.imageloading.data.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PexelsApi {

    @GET("curated")
    suspend fun getCuratedImages(@Query("page") page: Int) : Response<RemotePexelCuratedPhotos>

    @GET("search")
    suspend fun getRandomImages(@Query("page") page: Int, @Query("query") query: String) : Response<RemotePexelCuratedPhotos>

    @GET("videos/popular")
    suspend fun getPopularVideos(@Query("page") page: Int): Response<RemotePopularPexelVideos>
    companion object {
        const val BASE_URL = "https://api.pexels.com/v1/"
    }
}