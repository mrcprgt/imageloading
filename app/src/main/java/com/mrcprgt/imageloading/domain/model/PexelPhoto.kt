package com.mrcprgt.imageloading.domain.model

data class PexelPhoto(
    val id: Int,
    val photographer: String,
    val src: String,
    val avgColor: String,
    val altText: String
)