package com.mrcprgt.imageloading.domain.model

data class PexelVideo(
    val id : Int,
    val author: String,
    val link: String
)